TARGET := emu

emu: de2-115-emu.cpp *.sv
	(verilator --exe --build de2-115-emu.cpp -sv -cc top.sv -j $$(nproc) 1>/dev/null) || (verilator -sv -cc top.sv && g++ -Iobj_dir -I/usr/share/verilator/include de2-115-emu.cpp /usr/share/verilator/include/verilated.cpp obj_dir/Vtop.cpp obj_dir/Vtop__Syms.cpp -o $(TARGET))
	@cp obj_dir/Vtop ./$(TARGET)

clean:
	rm -rf obj_dir
	rm -f $(TARGET)