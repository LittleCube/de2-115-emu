#include <iostream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <unistd.h>
#include "Vtop.h"
#include "verilated.h"

#define module Vtop

#define flat(x, y) if ((~x >> y) & 1) { ss << " —   "; } else { ss << "     "; }

#define pipe(x, y, z) if ((~x >> y) & 1) { ss << "| "; } else { ss << "  "; } if ((~x >> z) & 1) { ss << "|  "; } else { ss << "   "; }

module *m;

typedef uint8_t u8;
typedef bool u;

using std::to_string;
using std::cout;
using std::flush;
using std::endl;
using std::stoi;

int main(int argc, char **argv)
{
	// Create an instance of our module
	m = new module;
	
	m->KEY = 0b1111;
	
	if (argc == 1)
	{
		printf("E: invalid arguments\n");
		printf("usage: %s <SW-value>\n", argv[0]);
		exit(-1);
	}
	
	else
	{
		m->SW = stoi(argv[1]) & 0x3FFFF;
	}
	
	while (true)
	{
		m->CLOCK_50 = 1;
		m->eval();
		
		m->CLOCK_50 = 0;
		m->eval();
		
		if (argc == 2)
		{
			std::stringstream ss;
			
			flat(m->HEX7, 0);
			flat(m->HEX6, 0);
			flat(m->HEX5, 0);
			flat(m->HEX4, 0);
			flat(m->HEX3, 0);
			flat(m->HEX2, 0);
			flat(m->HEX1, 0);
			flat(m->HEX0, 0);
			
			ss << endl;
			
			pipe(m->HEX7, 5, 1);
			pipe(m->HEX6, 5, 1);
			pipe(m->HEX5, 5, 1);
			pipe(m->HEX4, 5, 1);
			pipe(m->HEX3, 5, 1);
			pipe(m->HEX2, 5, 1);
			pipe(m->HEX1, 5, 1);
			pipe(m->HEX0, 5, 1);
			
			ss << endl;
			
			flat(m->HEX7, 6);
			flat(m->HEX6, 6);
			flat(m->HEX5, 6);
			flat(m->HEX4, 6);
			flat(m->HEX3, 6);
			flat(m->HEX2, 6);
			flat(m->HEX1, 6);
			flat(m->HEX0, 6);
			
			ss << endl;
			
			pipe(m->HEX7, 4, 2);
			pipe(m->HEX6, 4, 2);
			pipe(m->HEX5, 4, 2);
			pipe(m->HEX4, 4, 2);
			pipe(m->HEX3, 4, 2);
			pipe(m->HEX2, 4, 2);
			pipe(m->HEX1, 4, 2);
			pipe(m->HEX0, 4, 2);
			
			ss << endl;
			
			flat(m->HEX7, 3);
			flat(m->HEX6, 3);
			flat(m->HEX5, 3);
			flat(m->HEX4, 3);
			flat(m->HEX3, 3);
			flat(m->HEX2, 3);
			flat(m->HEX1, 3);
			flat(m->HEX0, 3);
			
			usleep(15);
			
			cout << "\33c" << ss.str() << endl;
		}
		
		else
		{
			cout << flush;
		}
		
		if (Verilated::gotFinish())
		{
			break;
		}
	}
	
	exit(EXIT_SUCCESS);
}