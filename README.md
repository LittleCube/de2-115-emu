# DE2-115 Emulator

An emulator using C++ and verilator that helps to test your SystemVerilog code before pushing to the board.

## Building

1. Install verilator.

```
sudo apt install verilator
```

2. Copy your top.sv and other SystemVerilog files to the root directory, along with any mem files (e.g. instmem.dat).

3. Make:

```
make
```

4. Run the newly compiled emulator with a given SW value (e.g. 61440) and an optional dummy argument (if you want to see your $display statements):

```
./emu <SW-value> [ show-$displays ]
```
